﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media.Imaging;

namespace BiaoBai
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        Dictionary<int,string> dic=new Dictionary<int, string>
        {
            {1,"做我女朋友好吗？"},
            {2,"钱我赚，家务活我干！"},
            {3,"房产证写你名,\n保证我妈会游泳！"},
            {4,"这还不同意？"},
            {5,"我开大众带字母！"},
        };
        private int i = 1;
        public MainWindow()
        {
            InitializeComponent();
            LoadData(i);
        }

        private void BtnNo_Click(object sender, RoutedEventArgs e)
        {
            i++;
            if (i==6)
            {
                i = 1;
            }

            LoadData(i);
        }

        void LoadData(int i)
        {
            img.Source=new BitmapImage(new Uri($"qiuai{i}.png", UriKind.Relative));
            txt.Text = dic[i];
        }
        private void BtnYes_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("我会爱你一辈子的！", "乖,真听话~", MessageBoxButton.OK, MessageBoxImage.Information);
            Application.Current.Shutdown();
        }

        private void BtnClose_Click(object sender, RoutedEventArgs e)
        {
           MessageBox.Show("门已经锁死了！","乖,听话啊~", MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}
